Sainsbury's Product page scanner test

It has been designed as a Maven project, so compile it and run its tests from the command line with...
mvn package

and then run it with:
java -jar target/FruitScanner-0.0.1-SNAPSHOT-jar-with-dependencies.jar <properly escaped url>

If you don't know how to properly escape the url, you can just run Main.main in Eclipse passing in the URL as a parameter.

To initialise the Eclipse project files:
mvn eclipse:eclipse