package fruitscanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is an utility class to scan pages from the Sainsbury's website and extract Product information
 * 
 * @author mdalco
 */
public class ProductPageScanner {
	/**
	 * Scans a product search page, navigate its links and extracts Product information.
	 * 
	 * @param url The search URL to scan
	 * @return a List of Product Items populated with the information scanned from the website
	 * @throws IOException if I/O errors occur trying to load the data
	 * @throws ParseException If some expected information is not numeric where it should
	 */
	public List<Product> scan(String url) throws IOException, ParseException {
		return scanPage(getPage(url));
	}

	/**
	 * Returns the list of products given the original search page.
	 * 
	 * @param page the page to analyse
	 * @return a list of products
	 * @throws IOException if an error occurs while loading the pages
	 * @throws ParseException if some information is not numeric as expected
	 */
	private List<Product> scanPage(Document page) throws IOException, ParseException {
		Element[] productAnchors = findProductAnchors(page);

		// This could have been done with the streaming API, but it's less readable, as it requires wrapping and re-throwing
		// checked exceptions to overcome design flaw in the API itself:
		// List<Product> products = Arrays.stream(productAnchors)
		//     .collect(
		//         ArrayList::new,
		//         (a, b) -> { try { a.add(getProductFromPage(getPage(b.attr("abs:href")))); } catch (Exception e) { throw new
		//             RuntimeException(e); } },
		//         (a, b) -> a.addAll(b));

		List<Product> products = new ArrayList<Product>();

		for (Element productAnchor : productAnchors)
			products.add(getProductFromProductPage(getPage(productAnchor.attr("abs:href"))));

		return products;
	}

	/**
	 * Scans a product page and returns a Product object representing it
	 * 
	 * @param page the page to scan
	 * @return a Product object representing the product at this page
	 * @throws ParseException if the unit price is not numeric
	 */
	private Product getProductFromProductPage(Document page) throws ParseException {
		String title = page.select(".productTitleDescriptionContainer").text();
		String description = null;
		String size = (page.body().text().length() / 1024) + "kb";
		// The description is found in elements of class "productText"
		// immediately following elements of class "productDataItemHeader",
		// and the only way to tell which "productText" is which information
		// without relying on its position is to look at the innerText
		// of the "productDataItemHeader" element
		List<Element> productDataItemHeaders = page.select(".productDataItemHeader");
		for (Element element : productDataItemHeaders) {
			final String text = element.text();
			switch (text) {
				case "Description":
					description = element.nextElementSibling().text();
					break;
			}
		}
		final String unitPriceText = page.select(".pricePerUnit").get(0).text();
		float unitPrice = NumberFormat.getInstance().parse(unitPriceText.substring(unitPriceText.indexOf("£") + 1)).floatValue();

		return new Product(title, size, unitPrice, description);
	}

	/**
	 * Finds the links to the products in the search page.
	 * 
	 * @param page The page to analyse
	 * @return an array of Element objects all representing the Anchors with the link to the single product pages.
	 */
	private Element[] findProductAnchors(Document page) {
		return page.select(".productInfo h3 a").toArray(new Element[] {});
	}

	/**
	 * Loads the page from the network at the give URL
	 * 
	 * @param url The URL to load
	 * @return A Document object representing the page DOM
	 * @throws IOException if an I/O error occurs trying to load the data
	 */
	private Document getPage(String url) throws IOException {
		return Jsoup.connect(url).get();
	}

}
