package fruitscanner;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProductsJSONPrinter {
	private final PrintStream printStream;

	public ProductsJSONPrinter(PrintStream printStream) {
		this.printStream = printStream;
	}

	/**
	 * Calculates the total and prints the list of products in the expected JSON format.
	 * 
	 * @param products The list of products to print
	 * @throws JsonGenerationException if an error happens while trying to format the data in JSON
	 * @throws JsonMappingException if an error happens while trying to format the data in JSON and there is no mapping
	 * @throws IOException if an I/O error happens while trying to print
	 */
	public void calculateTotalAndPrint(List<Product> products) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();

		Map<String, Object> output = new LinkedHashMap<>();
		output.put("results", products);
		output.put("total", products.stream().mapToDouble(Product::getUnitPrice).sum());
		printStream.println(mapper.writeValueAsString(output));
	}

}
