package fruitscanner;
import java.io.IOException;
import java.text.ParseException;

public class Main {

	/**
	 * Main entry point: expects exactly 1 parameter and simply invokes the two classes to fetch and print the data at the
	 * given URL
	 * 
	 * @param args Command-line parameters where only the first one is expected and must be a URL
	 * @throws IOException if any I/O error occurs during the operation
	 * @throws ParseException if some information expected to be numeric is not actually a number
	 */
	public static void main(String[] args) throws IOException, ParseException {
		if (args.length != 1) {
			System.out.println("This tool requires one argument, which is the URL of the page to scan.");
			System.exit(1);
		}
	
		System.out.println("Scanning, then printing: " + args[0]);
		new ProductsJSONPrinter(System.out).calculateTotalAndPrint(new ProductPageScanner().scan(args[0]));
	}

}
