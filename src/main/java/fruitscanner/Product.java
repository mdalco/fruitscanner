package fruitscanner;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Product POJO Being a POJO, this class has no logic, and methods are automatically generated
 * 
 * @author mdalco
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Product {
	private final String	title;
	private final String	size;
	private final float		unitPrice;
	private final String	description;

	public Product(String title, String size, float unitPrice, String description) {
		super();
		this.title = title;
		this.size = size;
		this.unitPrice = unitPrice;
		this.description = description;
	}

	@JsonProperty("unit_price")
	public float getUnitPrice() {
		return unitPrice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + Float.floatToIntBits(unitPrice);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (size == null) {
			if (other.size != null) {
				return false;
			}
		} else if (!size.equals(other.size)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		if (Float.floatToIntBits(unitPrice) != Float.floatToIntBits(other.unitPrice)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Product [title=" + title + ", size=" + size + ", unitPrice=" + unitPrice + ", description=" + description + "]";
	}

}
