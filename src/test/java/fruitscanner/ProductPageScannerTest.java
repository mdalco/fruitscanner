package fruitscanner;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductPageScannerTest {
	/** The Class Under Test */
	private ProductPageScanner	cut;
	/** A Stub server */
	private static Server				server;

	@BeforeClass
	public static void startServer() throws Exception {
		server = new Server(8080);
		server.setStopAtShutdown(true);
		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setContextPath("/");
		webAppContext.setResourceBase("src/test/resources");
		webAppContext.setClassLoader(ProductPageScannerTest.class.getClassLoader());
		server.addHandler(webAppContext);
		server.start();
	}

	@AfterClass
	public static void shutdownServer() throws Exception {
		server.stop();
	}

	@Before
	public void setUp() throws Exception {
		this.cut = new ProductPageScanner();
	}

	@Test
	public void testScanNoProducts() throws IOException, ParseException {
		@SuppressWarnings("unchecked") List<Product> products = Collections.EMPTY_LIST;
		assertEquals(products, cut.scan("http://localhost:8080/MainTestPageEmpty.html"));
	}

	@Test
	public void testScanTwoProducts() throws IOException, ParseException {
		List<Product> products = new ArrayList<>();
		products.add(new Product("Product 1", "3kb", 1.5f, "Description 1"));
		products.add(new Product("Product 2", "4kb", 1.8f, "Description 2"));
		assertEquals(products, cut.scan("http://localhost:8080/MainTestPage1.html"));
	}

}
