package fruitscanner;

import mockit.Injectable;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RunWith(JMockit.class)
public class ProductsJSONPrinterTest {
	private ProductsJSONPrinter			cut;

	@Injectable private PrintStream	printStream;

	@Before
	public void setUp() throws Exception {
		cut = new ProductsJSONPrinter(printStream);
	}

	@Test
	public void testNoProductsNoOutput() throws Exception {
		new StrictExpectations() {{
				printStream.println("{\"results\":[],\"total\":0.0}");
		}};

		@SuppressWarnings("unchecked")
		List<Product> products = Collections.EMPTY_LIST;
		cut.calculateTotalAndPrint(products);
	}

	@Test
	public void testPrintOneProduct() throws Exception {
		new StrictExpectations() {{
				printStream.println("{\"results\":[{\"title\":\"title1\",\"size\":\"size 1\",\"description\":\"description 1\",\"unit_price\":1.125}],\"total\":1.125}");
	  }};

		List<Product> products = new ArrayList<>();
		products.add(new Product("title1", "size 1", 1.125f, "description 1"));
		cut.calculateTotalAndPrint(products);
	}

	@Test
	public void testTwoProducts() throws Exception {
		new StrictExpectations() {{
				printStream
						.println("{\"results\":[{\"title\":\"title1\",\"size\":\"size 1\",\"description\":\"description 1\",\"unit_price\":1.125},{\"title\":\"title2\",\"size\":\"size 2\",\"description\":\"description 2\",\"unit_price\":2.25}],\"total\":3.375}");
		}};
		List<Product> products = new ArrayList<>();
		products.add(new Product("title1", "size 1", 1.125f, "description 1"));
		products.add(new Product("title2", "size 2", 2.25f, "description 2"));
		cut.calculateTotalAndPrint(products);
	}

	@Test
	public void testThreeProducts() throws Exception {
		new StrictExpectations() {{
				printStream
						.println("{\"results\":[{\"title\":\"title1\",\"size\":\"size 1\",\"description\":\"description 1\",\"unit_price\":1.125},{\"title\":\"title2\",\"size\":\"size 2\",\"description\":\"description 2\",\"unit_price\":2.25},{\"title\":\"title3\",\"size\":\"size 3\",\"description\":\"description 3\",\"unit_price\":3.375}],\"total\":6.75}");
		}};
		List<Product> products = new ArrayList<>();
		products.add(new Product("title1", "size 1", 1.125f, "description 1"));
		products.add(new Product("title2", "size 2", 2.25f, "description 2"));
		products.add(new Product("title3", "size 3", 3.375f, "description 3"));
		cut.calculateTotalAndPrint(products);
	}

}
